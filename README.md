# Registración de microservicios con Eureka #

Cuando desarrollamos en el marco de una arquitectura orientada a microservicios, donde tenemos muchos servicios interactuando entre sí, a través de por ejemplo servicios REST, surgen problemáticas inherentes a la misma, problemáticas que no tenemos al desarrollar aplicaciones monolíticas. En este tutorial nos vamos a ocupar de una en particular, y es que los microservicios deben conocer la ubicación de aquellos que necesite invocar. Imaginen una aplicación donde se relacionan cientos de servicios y en cada uno de ellos tengamos que ir manteniendo la ubicación de todos los que invoca o el agregado de nuevas instancias de los mismos para que la aplicación siga funcionando como esperamos.

Por suerte para nosotros, el modulo de Spring Cloud nos da herramientas para abordar este tipo de problemas y en el caso de nuestro problema en particular aparece Eureka como una solución simple para aplicar.

Eureka es un microservicio más dentro del ecosistema de la aplicación, que se encarga de registrar todos los servicios y sus instancias, para que luego cuando uno de ellos necesite invocar a otro, no deba conocer la ubicación del mismo sino solo el nombre del servicio y pedirle a Eureka la dirección.


### ¿Cómo funciona? ###

Una vez levantado el servidor de Eureka este quedará a la espera de que los demás microservicios se registren en él a través de un POST a una URL que expone y que se configura en cada cliente. Una vez registrados los servicios, quedan disponibles para que los demás puedan pedir a Eureka la dirección de alguna de sus instancias.

![diagrama-eureka.png](https://bytebucket.org/somospnt/techie-eureka/raw/9b7b170571ec2abf213f26de12a691a961cd1023/diagramas/diagrama-euraka.png?token=77d88cf568e8e5c161ce3b11485b81cdb9514970)

En el caso del diagrama, tenemos varias instancias de Eureka pero el funcionamiento es igual que cuando tenemos solo una instancia.

### Consola de Eureka ###

Si accedemos al host y al puerto donde está desplegado Euraka podremos ver una consola donde figuran todos los servicios y sus respectivas instancias, así cómo también información de la propia instancia de Eureka.

### Configuración del proyecto MAVEN ###

#### Empaquetar como JAR ####

```xml
<packaging>jar</packaging>
```

#### Spring boot 1.4 ####

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>1.4.0.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

#### Dependencias ####


* Tomcat embebido

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```


* Servidor de Eureka

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-eureka-server</artifactId>
</dependency>
```


* Spring Cloud

```xml
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>Brixton.SR5</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

#### Plugin maven spring boot ####

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
</plugin>
```

#### Configuración Java ####

```Java
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
    
}
```


#### Properties del proyecto ####

En el application.yml del proyecto podemos configurar Eureka server. 

```yml
server:
    port: 8761

eureka:
  client:
    registerWithEureka: false
    fetchRegistry: false
    server:
      waitTimeInMsWhenSyncEmpty: 0
```

La propertie del port es para definir el puerto donde se despliega el servidor de discovery y donde vamos a poder acceder a la consola en el path "/".

Las demás properties le indican a Eureka que no se registre a sí mismo.

Por último, es importante aclarar que este servicio es fundamental para el funcionamiento de toda una aplicación, sin el los servicios no se van a poder comunicar entre sí, por lo cual en producción debe tener alta disponibilidad para no tener problemas de este estilo.


### Configuración del cliente ###

Todo lo explicado hasta ahora es para configurar el servidor de Eureka y el código de ejemplo solo muestra esta parte, lo que explicaremos a continuación es para las aplicaciones que quieren registrarse y obtener direcciones de otros servicios.

La configuración del pom.xml es casi idéntica a la del servidor con la diferencia de la dependencia de Eureka, en lugar de spring-cloud-starter-eureka-server, debemos poner:

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
```

En el main del código en lugar de @EnableEurekaServer, debemos poner @EnableDiscoveryClient.

Lo último para configurar es el application.yml: 

* Nombre con el que se registra el servicio

```yml
spring:
  application:
    name: mi-servicio
```

* Ubicación del servidor de Eureka

```yml
eureka:
  client:
    serviceUrl:
      defaultZone: http:{host}:{puerto}/eureka/
```
 
#### Invocación entre servicios ####

Solo resta ver cómo conseguir la ubicación de un servicio desde otro. Acá es donde aparece la clase DiscoveryClient, la cual se encarga de buscar los servicios para luego poder acceder a la información del mismo. Lo vemos con un ejemplo de como obtener todas las instancias de un determinado servicio:

Primero instanciamos el discovery client

```java
@Autowired
private DiscoveryClient discoveryClient;
```

Luego le pedimos todas las instancias del servicio buscado, esto nos devuelve una lista de instancias, de esta puede obtener una en particular.

```java
ServiceInstance  miServicio = discoveryClient.getInstances("mi-servicio").get(0);
URI uri = miServicio.getUri();
```

Por último utilizando alguna librería para poder realizar peticiones REST invoco el servicio esperado, sin que el servicio conozco en ningún momento la ubicación de ningun otro.